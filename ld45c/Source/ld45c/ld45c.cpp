// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "ld45c.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ld45c, "ld45c" );
 