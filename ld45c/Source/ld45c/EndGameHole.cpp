// Fill out your copyright notice in the Description page of Project Settings.


#include "EndGameHole.h"



// Sets default values
AEndGameHole::AEndGameHole()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComp->SetGenerateOverlapEvents(true);

	RootComponent = MeshComp;

	OverlapBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("OverlapBoxComp"));
	OverlapBoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OverlapBoxComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	OverlapBoxComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	OverlapBoxComponent->SetBoxExtent(FVector(100.0f));


	OverlapBoxComponent->AttachTo(RootComponent);

	OverlapBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AEndGameHole::OnItemBeginOverlap);

}

// Called when the game starts or when spawned
void AEndGameHole::BeginPlay()
{
	Super::BeginPlay();
	GM = GetWorld()->GetAuthGameMode<Ald45cGameMode>();
	
}

// Called every frame
void AEndGameHole::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEndGameHole::OnItemBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	if (!OtherActor)
	{
		return;
	}

	Ald45cCharacter* character = Cast<Ald45cCharacter>(OtherActor);
	if (character)
	{

		GM->EndGameTrappedFlow();
		
	}
}

