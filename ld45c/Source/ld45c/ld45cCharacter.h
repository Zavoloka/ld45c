// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/Actor.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Item.h"
#include "Components/CapsuleComponent.h"
#include "ld45cCharacter.generated.h"

class USkeletalMeshComponent;

UCLASS(config=Game)
class Ald45cCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	Ald45cCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

protected:

	// https://answers.unrealengine.com/questions/375266/cant-use-pointer-for-tarray.html 
	// Bluprints dosn't work with pointers, so we don't use UPROPERTY()

	TArray<AItem*> * HeadSocketItems = nullptr;

	TArray<AItem*> *  BodySocketItems = nullptr; 

	//	LegsSocket, FootSocket;

	UPROPERTY()
	FName HeadSocketName = "headSocket";

	UPROPERTY()
	FName BodySocketName = "bodySocket";

	UPROPERTY()
	USkeletalMeshComponent* DerivedMeshComp;

	UPROPERTY()
	UCapsuleComponent* DerivedCapsuleComp;


	UPROPERTY()
	UCharacterMovementComponent * DerivedMovementComp;

	UPROPERTY()
	float InitCharacterWalkSpeedValue = 0.0f;

	UFUNCTION()
	void OnItemBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);




protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface


	FName& MapItemTypeToSocketName(const EItemType & ItemType);

	bool HandleItemAttachment(AItem * Item);
	bool HandleItemDetachment(EItemType ItemType, INT32 Index = -1);


public:

	virtual void BeginPlay() override;

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UFUNCTION()
	void AdjustSpeed(float value = 0.0f);

	void DropAllItems();
};

