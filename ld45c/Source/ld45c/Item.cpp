// Fill out your copyright notice in the Description page of Project Settings.

#include "Item.h"
#include "TimerManager.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AItem::AItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComp->SetGenerateOverlapEvents(true);

	RootComponent = MeshComp;

	OverlapSphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("OverlapSphereComponent"));
	OverlapSphereComponent->SetSphereRadius(100);
	OverlapSphereComponent->SetGenerateOverlapEvents(true);
	OverlapSphereComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	OverlapSphereComponent->SetupAttachment(MeshComp);

}

void AItem::DestroyDelegate()
{

}

// Called when the game starts or when spawned
void AItem::BeginPlay()
{
	Super::BeginPlay();
	//FuzeTimerHandle;
}

// Called every frame
void AItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AItem::DestroyObjectByTime(float Seconds)
{

	Destroy(false, false);
	//GetWorld()->GetTimerManager().SetTimer(TimerHandler, this, &AItem::Destroy, Seconds);

}

