// Fill out your copyright notice in the Description page of Project Settings.

#include "EndGameFinish.h"

// Sets default values
AEndGameFinish::AEndGameFinish()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComp->SetGenerateOverlapEvents(true);

	RootComponent = MeshComp;

	OverlapSphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("OverlapSphereComponent"));
	OverlapSphereComponent->SetSphereRadius(100);
	OverlapSphereComponent->SetGenerateOverlapEvents(true);
	OverlapSphereComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	OverlapSphereComponent->SetupAttachment(MeshComp);


	OverlapSphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AEndGameFinish::OnItemBeginOverlap);

}

// Called when the game starts or when spawned
void AEndGameFinish::BeginPlay()
{
	Super::BeginPlay();
	GM = GetWorld()->GetAuthGameMode<Ald45cGameMode>();
	
}

// Called every frame
void AEndGameFinish::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEndGameFinish::OnItemBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!OtherActor)
	{
		return;
	}

	Ald45cCharacter* character = Cast<Ald45cCharacter>(OtherActor);
	if (character)
	{

		GM->EndGameFinishFlow();
	}

}

