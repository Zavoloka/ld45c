// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "ld45cGameMode.h"
#include "ld45cCharacter.h"
#include "UObject/ConstructorHelpers.h"
//#include "Blueprint/UserWidget.h"
//#include "ThirdPersonCPP/UI"

Ald45cGameMode::Ald45cGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
