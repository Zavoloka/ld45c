// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "ld45cCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/StaticMeshComponent.h"


//////////////////////////////////////////////////////////////////////////
// Ald45cCharacter

Ald45cCharacter::Ald45cCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	// Init derived Mesh and Capsule components
	DerivedMeshComp = GetMesh();

	DerivedCapsuleComp = GetCapsuleComponent();
	DerivedCapsuleComp->OnComponentBeginOverlap.AddDynamic(this, &Ald45cCharacter::OnItemBeginOverlap);
	DerivedMovementComp = GetCharacterMovement();

	HeadSocketItems = new TArray<AItem *>();
	BodySocketItems = new TArray<AItem *>();
}

//////////////////////////////////////////////////////////////////////////
// Input

void Ald45cCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);



	PlayerInputComponent->BindAxis("MoveForward", this, &Ald45cCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &Ald45cCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &Ald45cCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &Ald45cCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &Ald45cCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &Ald45cCharacter::TouchStopped);

	// VR headset functionality
	//PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &Ald45cCharacter::DropAllItems);
}


FName& Ald45cCharacter::MapItemTypeToSocketName(const EItemType & ItemType)
{

	switch (ItemType)
	{
		case EItemType::IT_Head : return HeadSocketName;
		case EItemType::IT_Body : return BodySocketName;
	}

	return BodySocketName;
}

bool Ald45cCharacter::HandleItemAttachment(AItem * Item)
{

	auto result = Item->MeshComp->AttachToComponent(DerivedMeshComp, FAttachmentTransformRules::SnapToTargetNotIncludingScale, MapItemTypeToSocketName(Item->ItemType));
	if (result)
	{
	
		switch (Item->ItemType)
		{
			// DEVTODO: refactor + handle result
			case (EItemType::IT_Head): 	HeadSocketItems->Add(Item); break;   // const_cast<AItem*>
			case (EItemType::IT_Body): 	BodySocketItems->Add(Item); break;
		}
	
		Item->IsWeared = true;
		AdjustSpeed();

	}
	else {
		return false;
	}

	return result; //&& addResult

}

bool Ald45cCharacter::HandleItemDetachment(EItemType ItemType, INT32 Index)
{

		TArray<AItem*> * socketItemsTorage = nullptr;
		switch (ItemType)
		{

			case (EItemType::IT_Head): socketItemsTorage = HeadSocketItems; break;
			case (EItemType::IT_Body): socketItemsTorage = BodySocketItems; break;
		}


		if (socketItemsTorage == nullptr)
		{
			return false;
		}


		for (auto & item : (*socketItemsTorage))
		{
			item->DetachAllSceneComponents(DerivedMeshComp, FDetachmentTransformRules::KeepWorldTransform);
			item->IsWeared = false;

			item->DestroyObjectByTime(2);

			// DEV_TODO: Add random world shift or destroy

		}

		socketItemsTorage->Empty();
		socketItemsTorage = nullptr;

		auto headCount = HeadSocketItems->Num();
		auto bodyCount = BodySocketItems->Num();

		UE_LOG(LogTemp, Warning, TEXT("Destrucion head & body count : %d , %d"), headCount, bodyCount);

		return true;
}


void Ald45cCharacter::BeginPlay()
{
	Super::BeginPlay();
	InitCharacterWalkSpeedValue = DerivedMovementComp->MaxWalkSpeed;

}

void Ald45cCharacter::AdjustSpeed(float value /*= 0.0f*/)
{

	// sum all items and apply it to character speed 
	// include value as it might come from outside (police)

	//InitCharacterWalkSpeedValue
	

	// SUM Head
	float totalWeight = 0.0f;

	for (const auto & item : (*HeadSocketItems))
	{
		totalWeight += item->Weight;
	}

	for (const auto & item : (*BodySocketItems))
	{
		totalWeight += item->Weight;
	}

	totalWeight += value;
	DerivedMovementComp->MaxWalkSpeed -= totalWeight;

	auto logspeed = DerivedMovementComp->MaxWalkSpeed;

	UE_LOG(LogTemp, Warning, TEXT("Current speed : %f"), logspeed);

}

void Ald45cCharacter::OnItemBeginOverlap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{

	if (!OtherActor) 
	{ 
		return; 
	}

	AItem* Item = Cast<AItem>(OtherActor);
	if (Item && !Item->IsWeared) 
	{

		Ald45cCharacter::HandleItemAttachment(Item);
		return;
	}
}

void Ald45cCharacter::OnResetVR()
{
	
}

void Ald45cCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void Ald45cCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void Ald45cCharacter::DropAllItems()
{
	//UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();

	UE_LOG(LogTemp, Warning, TEXT("ResetVR!"));

	HandleItemDetachment(EItemType::IT_Head, -1);
	HandleItemDetachment(EItemType::IT_Body, -1);

	DerivedMovementComp->MaxWalkSpeed = InitCharacterWalkSpeedValue;

	auto logspeed = DerivedMovementComp->MaxWalkSpeed;

	UE_LOG(LogTemp, Warning, TEXT("All items gone speed : %f"), logspeed);

}

void Ald45cCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void Ald45cCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void Ald45cCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void Ald45cCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
