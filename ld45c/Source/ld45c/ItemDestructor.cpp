// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemDestructor.h"
#include "ld45cCharacter.h"


// Sets default values
AItemDestructor::AItemDestructor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;



	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComp->SetGenerateOverlapEvents(true);

	RootComponent = MeshComp;

	OverlapSphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("OverlapSphereComponent"));
	OverlapSphereComponent->SetSphereRadius(100);
	OverlapSphereComponent->SetGenerateOverlapEvents(true);
	OverlapSphereComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	OverlapSphereComponent->SetupAttachment(MeshComp);


	OverlapSphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AItemDestructor::OnItemBeginOverlap);
}

// Called when the game starts or when spawned
void AItemDestructor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AItemDestructor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AItemDestructor::OnItemBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	if (!OtherActor)
	{
		return;
	}

	Ald45cCharacter* character = Cast<Ald45cCharacter>(OtherActor);
	if (character)
	{
		character->DropAllItems();

	}

}