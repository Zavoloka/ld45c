// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ld45cGameMode.generated.h"

UCLASS(minimalapi)
class Ald45cGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected: 

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> wMainMenu;

	UUserWidget* MyMainMenu;

public:
	Ald45cGameMode();

	//TArray<> * CheckpointList;

	UFUNCTION(BlueprintImplementableEvent, Category= "UI Event")
	void EndGameTrappedFlow();

	UFUNCTION(BlueprintImplementableEvent, Category = "UI Event")
	void EndGameBustedFlow();

	UFUNCTION(BlueprintImplementableEvent, Category = "UI Event")
    void EndGameFinishFlow();

};



