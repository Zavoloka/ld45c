// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Item.generated.h"

UENUM(BlueprintType)
enum class EItemType : uint8
{
	IT_Head		UMETA(DisplayName = "Head item"),
	IT_Body		UMETA(DisplayName = "Body item")
};


UCLASS()
class LD45C_API AItem : public AActor
{
	GENERATED_BODY()

	
public:	

	// Visual representation
	UPROPERTY(EditAnywhere, Category = "Components")
	UStaticMeshComponent* MeshComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	USphereComponent* OverlapSphereComponent;


	UPROPERTY(EditAnywhere, Category = "Components")
	EItemType ItemType = EItemType::IT_Body;		// Body type by default;

	UPROPERTY()
	bool IsWeared;

	UPROPERTY(EditAnywhere,  Category = "Components")
	float Weight = 1.0f;


	// Sets default values for this actor's properties
	AItem();

protected:

	// https://www.tomlooman.com/using-timers-in-ue4/
	FTimerHandle TimerHandler;

	void DestroyDelegate();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void DestroyObjectByTime(float Seconds);

};
