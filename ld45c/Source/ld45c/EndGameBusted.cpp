// Fill out your copyright notice in the Description page of Project Settings.


#include "EndGameBusted.h"

// Sets default values
AEndGameBusted::AEndGameBusted()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEndGameBusted::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEndGameBusted::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

