// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "ld45cCharacter.h"
#include "ld45cGameMode.h"
#include "EndGameHole.generated.h"

UCLASS()
class LD45C_API AEndGameHole : public AActor
{
	GENERATED_BODY()
	
public:	

	// Visual representation
	UPROPERTY(EditAnywhere, Category = "Components")
		UStaticMeshComponent* MeshComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		UBoxComponent * OverlapBoxComponent;


	UFUNCTION()
		void OnItemBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	// Sets default values for this actor's properties
	AEndGameHole();

protected:

	Ald45cGameMode * GM;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
